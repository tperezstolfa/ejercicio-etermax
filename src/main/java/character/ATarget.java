package character;

public abstract class ATarget {

	protected int healthPoints;
	protected boolean alive;
	/** I'll supose that this will be a 2D game in only one axis, so position can be an int starting from 0. */
	protected int position;

	
	public int getHealthPoints() {
		return healthPoints;
	}
	
	public void setHealthPoints(int healthPoints) {
		this.healthPoints = healthPoints;
	}
	
	public boolean isAlive() {
		return alive;
	}
	
	public void setAlive(boolean alive) {
		this.alive = alive;
	}

	public int getPosition() {
		return position;
	}
	
	public void setPosition(int position) {
		this.position = position;
	}
	public abstract void receiveHit(Character attacker);

	public abstract boolean canBeTargetOf(Character attacker);

	/** Returns true if the target is in range of given attacker. */
	public boolean isInRangeOf(Character attacker) {
		
		int distanceToAttacker = Math.abs(attacker.getPosition() - this.getPosition());
		int attackerRange = attacker.getRange();
		return attackerRange >= distanceToAttacker;
	}
	
	/** Tries to hit the target, if attacker can hit the target, then the method receiveHit is called */
	public void receiveAttack(Character attacker) {
		
		if(canBeTargetOf(attacker) && isInRangeOf(attacker)) {
			receiveHit(attacker);
		}
	}
	
	public void receiveDamage(int receivedDamage) {
		
		this.healthPoints -= receivedDamage;
		if(this.healthPoints <= 0) {
			this.healthPoints = 0;
			this.alive = false;
		}
	}
}
