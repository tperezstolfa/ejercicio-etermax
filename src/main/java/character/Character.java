package character;

import java.util.HashSet;
import java.util.Set;

import faction.Faction;
import weapon.Weapon;
import weapon.WeaponsFactory;

public class Character extends ATarget {

	public static float DAMAGE_MODIFIER_MORE = 1.5f;
	public static float DAMAGE_MODIFIER_LESS = 0.5f;
	public static int LEVEL_DIFFERENCE_TO_MODIFY_DAMAGE_MORE = 5;
	public static int LEVEL_DIFFERENCE_TO_MODIFY_DAMAGE_LESS = 5;
	public static int BASE_HEALING_POWER = 100;
	public static int MAX_HP = 1000;
	public static int START_LEVEL = 1;
	public static boolean START_ALIVE = true;
	
	private int level;
	private Weapon weapon;
	private int healingPower;
	Set<Faction> factions;
	
	public Character() {
		setLevel(START_LEVEL);
		setHealthPoints(MAX_HP);
		setAlive(START_ALIVE);
		setWeapon(WeaponsFactory.getHumanFists());
		setHealingPower(BASE_HEALING_POWER);
		setPosition(0);
		setFactions(new HashSet<Faction>());
	}
	
	public Character(int position) {
		setLevel(START_LEVEL);
		setHealthPoints(MAX_HP);
		setAlive(START_ALIVE);
		setWeapon(WeaponsFactory.getHumanFists());
		setHealingPower(BASE_HEALING_POWER);
		setPosition(position);
		setFactions(new HashSet<Faction>());
	}
	
	public Character(Weapon weapon) {
		setLevel(START_LEVEL);
		setHealthPoints(MAX_HP);
		setAlive(START_ALIVE);
		setWeapon(weapon);
		setHealingPower(BASE_HEALING_POWER);
		setPosition(0);
		setFactions(new HashSet<Faction>());
	}
	
	public int getLevel() {
		return level;
	}
	
	public void setLevel(int level) {
		this.level = level;
	}
	
	public Weapon getWeapon() {
		return weapon;
	}

	public void setWeapon(Weapon weapon) {
		this.weapon = weapon;
	}
	
	public int getBaseAttack() {
		return weapon.getAttack();
	}
	
	public Set<Faction> getFactions() {
		return factions;
	}

	public void setFactions(Set<Faction> factions) {
		this.factions = factions;
	}

	public boolean hasHealthPoints(int healthPointsToCheck) {

		return this.getHealthPoints() == healthPointsToCheck;
	}

	public boolean hasLevel(int levelToCheck) {

		return this.getLevel() == levelToCheck;
	}

	public boolean attackIsGreaterThan(int attackToCheck) {

		return this.getBaseAttack() > attackToCheck;
	}

	public boolean hasAttackPower() {

		return attackIsGreaterThan(0);
	}

	public boolean isDead() {

		return !this.isAlive();
	}

	public boolean hasHealingPower() {

		return this.healingPowerIsGreaterThan(0);
	}

	public boolean healingPowerIsGreaterThan(int healingPower) {

		return this.getHealingPower() > healingPower;
	}
	
	public int getAttackOverTarget(Character target) {
		
		int levelDifference = this.getLevelDifference(target);
		float modifier = 1;
		if(levelDifference >= LEVEL_DIFFERENCE_TO_MODIFY_DAMAGE_MORE) {
			modifier = DAMAGE_MODIFIER_MORE;
		} else if(levelDifference <= -LEVEL_DIFFERENCE_TO_MODIFY_DAMAGE_LESS) {
			modifier = DAMAGE_MODIFIER_LESS;
		}
		
		return (int) (getBaseAttack() * modifier);
	}

	public int getLevelDifference(Character target) {
		
		return this.getLevel() - target.getLevel();
	}

	public int getHealingPower() {
		return healingPower;
	}
	
	public void setHealingPower(int healingPower) {
		this.healingPower = healingPower;
	}
	
	/** Deals damage to the given target and returns the target after attacking */
	public ATarget attack(ATarget enemy) {

		enemy.receiveAttack(this);

		return enemy;
	}
	
	@Override
	public void receiveHit(Character attacker) {
		
		this.receiveDamage(attacker.getAttackOverTarget(this));
	}

	@Override
	public boolean canBeTargetOf(Character attacker) {
		return !this.isAlly(attacker) && this.isInRangeOf(attacker);
	}

	public int getRange() {

		return this.getWeapon().getRange();
	}
	
	public Character heal(Character ally) {
		
		if(canHeal(ally)) {			
			ally.receiveHealing(this.getHealingPower());
		}
		return ally;
	}

	private boolean canHeal(Character ally) {
		return ally.isAlive() && this.isAlly(ally);
	}

	public void receiveHealing(int receivedHealing) {
		this.healthPoints += receivedHealing;
		if(this.healthPoints >= MAX_HP) {
			this.healthPoints = MAX_HP;
		}
	}
	
	/** Returns if the given character is an ally, in this simple game the only ally is the same character, 
	I check if it is the same instance but if the game was more complex and the characters had some kind of identifier
	it would be better to use that, I named the method "isAlly" because it makes sense to me that if you had allies you could heal them */
	public boolean isAlly(Character possibleAlly) {
		
		boolean isAlly = possibleAlly == this;
		
		if(isAlly) {
			return true;
		}
		for (Faction faction : factions) {
			if(faction.hasMember(possibleAlly)) {
				return true;
			}
		}
		return isAlly;
	}

	public void joinFaction(Faction faction) {
		
		this.addFaction(faction);
	}

	private void addFaction(Faction faction) {

		factions.add(faction);
		faction.addMember(this);		
	}

	public boolean isMemberOf(Faction faction) {
		
		return this.getFactions().contains(faction);
	}
	
	/** Returns true if the character is member of all the given Factions */
	public boolean isMemberOf(Set<Faction> factions) {
		
		for (Faction faction : factions) {
			if(!this.isMemberOf(faction)) return false;
		}
		return true;
	}

	public void leaveFaction(Faction faction) {
		
		if(isMemberOf(faction)) {
			this.getFactions().remove(faction);
		}
	}

	public boolean hasFaction() {

		return this.getFactions().size() > 0;
	}

}
