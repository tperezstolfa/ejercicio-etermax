package character;

public class Prop extends ATarget {

	public static int TREE_HEALTH_POINTS = 2000;
	
	public Prop(int healthPoints) {
		setHealthPoints(healthPoints);
		setAlive(true);
		setPosition(0);
	}
	
	public boolean isDestroyed() {
		
		return !this.isAlive();
	}
	
	@Override
	public boolean canBeTargetOf(Character attacker) {
		
		return this.isInRangeOf(attacker);
	}
	
	@Override
	public void receiveHit(Character attacker) {
		
		this.receiveDamage(attacker.getBaseAttack());
	}
	
}
