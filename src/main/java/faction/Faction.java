package faction;

import java.util.ArrayList;
import java.util.List;

import character.Character;

public class Faction {

	String name;
	List<Character> members;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Character> getMembers() {
		return members;
	}

	public void setMembers(List<Character> members) {
		this.members = members;
	}
	
	public Faction(String name) {
		this.setName(name);
		this.setMembers(new ArrayList<Character>());
	}
	
	public void addMember(Character newMember) {
		this.members.add(newMember);
	}

	public boolean hasMember(Character player) {

		return this.getMembers().contains(player);
	}
}
