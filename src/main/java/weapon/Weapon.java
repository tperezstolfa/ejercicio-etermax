package weapon;


public class Weapon {
	
	public static int RANGE_DISTANCE = 20;
	public static int RANGE_MELEE = 2;
	
	private int attack;
	private int range;
	
	public Weapon(int attack) {
		setAttack(attack);
		setRange(RANGE_MELEE);
	}

	public Weapon(int attack, int range) {
		setAttack(attack);
		setRange(range);
	}

	public int getAttack() {
		return attack;
	}

	public void setAttack(int attack) {
		this.attack = attack;
	}

	public int getRange() {
		return range;
	}

	public void setRange(int range) {
		this.range = range;
	}
}
