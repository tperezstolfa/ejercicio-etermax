package weapon;

import character.Character;
import character.Prop;

public class WeaponsFactory {


	public static int DAMAGE_NO_WEAPON = 10;
	public static int DAMAGE_WEAK_WEAPON = 50;
	public static int DAMAGE_AVERAGE_WEAPON = 100;
	public static int DAMAGE_STRONG_WEAPON = 200;
	
	public static Weapon getHumanFists() {
		return new Weapon(DAMAGE_NO_WEAPON);
	}
	
	public static Weapon getStrongMeleeWeapon() {
		return new Weapon(DAMAGE_STRONG_WEAPON);
	}

	public static Weapon getAverageMeleeWeapon() {
		return new Weapon(DAMAGE_AVERAGE_WEAPON);
	}

	public static Weapon getWeakMeleeWeapon() {
		return new Weapon(DAMAGE_WEAK_WEAPON);
	}
	
	public static Weapon getLegendaryPoisonedBladeOfDeath() {
		return new Weapon(Character.MAX_HP);
	}
	
	public static Weapon getSuperBow() {
		return new Weapon(DAMAGE_STRONG_WEAPON, Weapon.RANGE_DISTANCE);
	}
	
	public static Weapon getTreeBreaker() {
		return new Weapon(Prop.TREE_HEALTH_POINTS, Weapon.RANGE_DISTANCE);
	}
}
