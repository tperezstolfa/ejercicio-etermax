package character;

import org.junit.Assert;
import org.junit.Test;

import weapon.Weapon;
import weapon.WeaponsFactory;

public class CharacterTest {	
	
	@Test
	public void characterInitialHpWhenCreated() {
		
		Character player = new Character();
		
		Assert.assertTrue(player.hasHealthPoints(Character.MAX_HP));
	}

	@Test
	public void characterInitialLevelWhenCreated() {
		
		Character player = new Character();
		
		Assert.assertTrue(player.hasLevel(Character.START_LEVEL));
	}
	
	@Test
	public void characterAliveStateWhenCreated() {
		
		Character player = new Character();
		
		Assert.assertTrue(player.isAlive());
	}
	
	@Test
	public void characterWeaponChangeChangesAttack() {
		
		Character player = new Character(WeaponsFactory.getAverageMeleeWeapon());
		int firstPlayerAttack = player.getBaseAttack();
		
		player.setWeapon(WeaponsFactory.getStrongMeleeWeapon());
		
		Assert.assertTrue(player.attackIsGreaterThan(firstPlayerAttack));
	}
	
	@Test
	public void characterDealsDamageCorrectlyToEnemy() {
		
		Character player = new Character(WeaponsFactory.getWeakMeleeWeapon());
		Character enemy = new Character();
		int enemyInitialHp = enemy.getHealthPoints();
		int playerDamage = player.getBaseAttack();
		
		player.attack(enemy);
		
		Assert.assertTrue(player.hasAttackPower() && enemy.hasHealthPoints(enemyInitialHp - playerDamage));
	}
	
	@Test
	public void charactersCanDie() {
		
		Character player = new Character(WeaponsFactory.getLegendaryPoisonedBladeOfDeath());
		Character enemy = new Character();
		
		player.attack(enemy);
		
		Assert.assertFalse(enemy.isAlive());
	}
	
	@Test
	public void characterHealsCorrectlyHimself() {
		
		Character player = new Character();
		int playerInitialHp = 100;
		int playerHealingPower = player.getHealingPower();
		
		player.setHealthPoints(playerInitialHp);
		player.heal(player);
		
		Assert.assertTrue(player.hasHealthPoints(playerInitialHp + playerHealingPower));
	}

	@Test
	public void characterHealthCantGoOverMax() {
		
		Character player = new Character();
		int playerInitialHp = 100;
		int playerHealingPower = Character.MAX_HP * 2;
		
		player.setHealingPower(playerHealingPower);
		player.setHealthPoints(playerInitialHp);
		player.heal(player);
		
		Assert.assertTrue(player.hasHealthPoints(Character.MAX_HP));
	}

	@Test
	public void deadCharacterCantBeHealed() {
		
		Character player = new Character();
		
		//Set dead player
		player.setAlive(false);
		player.setHealthPoints(0);
		
		player.heal(player);
		
		Assert.assertTrue(player.hasHealingPower() && player.hasHealthPoints(0) && player.isDead());
	}
	
	@Test
	public void characterCanNotDealDamageToHimself() {
		
		Character player = new Character();
		int playerInitialHp = player.getHealthPoints();
		
		player.attack(player);
		
		Assert.assertTrue(player.hasAttackPower() && player.hasHealthPoints(playerInitialHp));
	}

	@Test
	public void characterCanNotHealEnemies() {
		
		Character player = new Character();
		Character enemy = new Character();
		int enemyInitialHp = 100;
		enemy.setHealthPoints(enemyInitialHp);
		
		player.heal(enemy);
		
		Assert.assertTrue(player.hasHealingPower() && enemy.hasHealthPoints(enemyInitialHp));
	}
	
	@Test
	public void character5LevelsBelowDealsLessDamage() {
		Character level10Character = new Character();
		level10Character.setLevel(10);
		Character level15Character = new Character();
		level15Character.setLevel(15);
		
		int level15CharacterInitialHp = level15Character.getHealthPoints();
		int level10CharacterModifiedAttack = (int) (level10Character.getBaseAttack() * Character.DAMAGE_MODIFIER_LESS);
		
		level10Character.attack(level15Character);
		
		Assert.assertTrue(level10Character.hasAttackPower() && level15Character.hasHealthPoints(level15CharacterInitialHp - level10CharacterModifiedAttack));
	}

	@Test
	public void character5LevelsAboveDealsMoreDamage() {
		Character level10Character = new Character();
		level10Character.setLevel(10);
		Character level15Character = new Character();
		level15Character.setLevel(15);
		
		int level10CharacterInitialHp = level10Character.getHealthPoints();
		int level15CharacterModifiedAttack = (int) (level15Character.getBaseAttack() * Character.DAMAGE_MODIFIER_MORE);
		
		level15Character.attack(level10Character);
		
		Assert.assertTrue(level15Character.hasAttackPower() && level10Character.hasHealthPoints(level10CharacterInitialHp - level15CharacterModifiedAttack));
	}
	
	@Test
	public void rangedAttackWithinRangeHits() {
		
		Character player = new Character(WeaponsFactory.getSuperBow());
		Character enemy = new Character();
		player.setPosition(0);
		enemy.setPosition(Weapon.RANGE_DISTANCE);
		
		int enemyInitialHp = enemy.getHealthPoints();
		int playerDamage = player.getBaseAttack();
		
		player.attack(enemy);
		
		Assert.assertTrue(player.hasAttackPower() && enemy.hasHealthPoints(enemyInitialHp - playerDamage));
	}
	
	@Test
	public void rangedAttackNotInRangeDoesntHit() {
		
		Character player = new Character(WeaponsFactory.getSuperBow());
		Character enemy = new Character();
		player.setPosition(0);
		enemy.setPosition(Weapon.RANGE_DISTANCE + 1);
		
		int enemyInitialHp = enemy.getHealthPoints();
		
		player.attack(enemy);
		
		Assert.assertTrue(player.hasAttackPower() && enemy.hasHealthPoints(enemyInitialHp));
	}
	
	@Test
	public void meleeAttackWithinRangeHits() {
		
		Character player = new Character(WeaponsFactory.getAverageMeleeWeapon());
		Character enemy = new Character();
		player.setPosition(0);
		enemy.setPosition(Weapon.RANGE_MELEE);
		
		int enemyInitialHp = enemy.getHealthPoints();
		int playerDamage = player.getBaseAttack();
		
		player.attack(enemy);
		
		Assert.assertTrue(player.hasAttackPower() && enemy.hasHealthPoints(enemyInitialHp - playerDamage));
	}
	
	@Test
	public void meleeAttackNotInRangeDoesntHit() {
		
		Character player = new Character(WeaponsFactory.getAverageMeleeWeapon());
		Character enemy = new Character();
		player.setPosition(0);
		enemy.setPosition(Weapon.RANGE_MELEE + 1);
		
		int enemyInitialHp = enemy.getHealthPoints();
		
		player.attack(enemy);
		
		Assert.assertTrue(player.hasAttackPower() && enemy.hasHealthPoints(enemyInitialHp));
	}
	


}
