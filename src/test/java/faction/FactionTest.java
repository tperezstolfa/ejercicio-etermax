package faction;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import character.Character;

public class FactionTest {

		@Test
		public void characterCanJoinFactionAndWillBePartOfIt() {

			String factionName = "Dragon Knights";
			Faction faction = new Faction(factionName);
			Character player = new Character();
			
			player.joinFaction(faction);
			
			Assert.assertTrue(faction.hasMember(player) && player.isMemberOf(faction));
		}
		
		@Test
		public void characterCanJoinMoreThanOneFaction() {

			String faction1Name = "Dragon Knights";
			String faction2Name = "Little Mermaid Fans";
			Faction faction1 = new Faction(faction1Name);
			Faction faction2 = new Faction(faction2Name);
			Set<Faction> factionsToJoin = new HashSet<Faction>();
			factionsToJoin.add(faction1);
			factionsToJoin.add(faction2);
			Character player = new Character();
			
			player.joinFaction(faction1);
			player.joinFaction(faction2);
			
			Assert.assertTrue(player.isMemberOf(factionsToJoin));
		}

		@Test
		public void characterDoesntBelongToFactionsWhenCreated() {
			
			Character player = new Character();
			
			Assert.assertFalse(player.hasFaction());
		}
		
		@Test
		public void characterCanLeaveAFaction() {
			
			Character player = new Character();
			String faction1Name = "Valar Morghulis";
			Faction faction = new Faction(faction1Name);
			player.joinFaction(faction);
			
			player.leaveFaction(faction);
			
			Assert.assertFalse(player.isMemberOf(faction));
		}

		@Test
		public void characterCantLeaveAFactionIfHeDoesntBelongToIt() {
			
			Character player = new Character();
			String faction1Name = "Valar Morghulis";
			Faction faction = new Faction(faction1Name);
			
			player.leaveFaction(faction);
			
			Assert.assertFalse(player.isMemberOf(faction));
		}
		
		@Test
		public void charactersFromSameFactionCanHealEachOther() {
			
			Character player = new Character();
			Character ally = new Character();
			String faction1Name = "Valar Morghulis";
			Faction faction = new Faction(faction1Name);
			player.joinFaction(faction);
			ally.joinFaction(faction);

			int allyInitialHp = 100;
			int playerHealingPower = player.getHealingPower();
			
			ally.setHealthPoints(allyInitialHp);
			player.heal(ally);
			
			Assert.assertTrue(ally.hasHealthPoints(allyInitialHp + playerHealingPower));
		}
		
		@Test
		public void charactersFromSameFactionCantAttackEachOther() {
			
			Character player = new Character();
			Character ally = new Character();
			String faction1Name = "Valar Morghulis";
			Faction faction = new Faction(faction1Name);
			player.joinFaction(faction);
			ally.joinFaction(faction);

			int allyInitialHp = ally.getHealthPoints();
			
			player.attack(ally);
			
			Assert.assertTrue(player.hasAttackPower() && ally.hasHealthPoints(allyInitialHp));
		}
		
}
