package prop;

import org.junit.Assert;
import org.junit.Test;

import character.Character;
import character.Prop;
import weapon.WeaponsFactory;

public class PropTest {

	@Test
	public void propCanBeDestroyedByCharacter() {
		
		Character player = new Character(WeaponsFactory.getTreeBreaker());
		Prop tree = new Prop(Prop.TREE_HEALTH_POINTS);
		
		player.attack(tree);
		
		Assert.assertTrue(tree.isDestroyed());
	}
}
