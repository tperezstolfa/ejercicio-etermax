package weapon;

import org.junit.Assert;
import org.junit.Test;

import character.Character;

public class WeaponsFactoryTest {
	
	@Test
	public void humanFistsAttack() {
		Weapon humanFists = WeaponsFactory.getHumanFists();
		Assert.assertTrue(humanFists.getAttack() == WeaponsFactory.DAMAGE_NO_WEAPON);
	}
	
	@Test
	public void strongWeaponAttack() {
		Weapon strongWeapon = WeaponsFactory.getStrongMeleeWeapon();
		Assert.assertTrue(strongWeapon.getAttack() == WeaponsFactory.DAMAGE_STRONG_WEAPON);
	}
	
	@Test
	public void averageWeaponAttack() {
		Weapon averageWeapon = WeaponsFactory.getAverageMeleeWeapon();
		Assert.assertTrue(averageWeapon.getAttack() == WeaponsFactory.DAMAGE_AVERAGE_WEAPON);
	}
	
	@Test
	public void weakWeaponAttack() {
		Weapon weakWeapon = WeaponsFactory.getWeakMeleeWeapon();
		Assert.assertTrue(weakWeapon.getAttack() == WeaponsFactory.DAMAGE_WEAK_WEAPON);
	}
	
	@Test
	public void legendaryWeaponAttack() {
		Weapon legendaryWeapon = WeaponsFactory.getLegendaryPoisonedBladeOfDeath();
		Assert.assertTrue(legendaryWeapon.getAttack() == Character.MAX_HP);
	}

	@Test
	public void rangedWeaponDistance() {
		Weapon rangedWeapon = WeaponsFactory.getSuperBow();
		Assert.assertTrue(rangedWeapon.getRange() == Weapon.RANGE_DISTANCE);
	}

}
